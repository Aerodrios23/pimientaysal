import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactoComponent } from './main/Contacto/contacto/contacto.component';
import { DescargablesComponent } from './main/Descargables/descargables/descargables.component';
import { IndexComponent } from './main/Index/index/index.component';
import { ServiciosComponent } from './main/Servicios/servicios/servicios.component';
import { SuscripcionComponent } from './main/Suscribete/suscripcion/suscripcion.component';
import { TiendaComponent } from './main/Tienda/tienda/tienda.component';


const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'Index', component: IndexComponent},
  {path: 'Descargables', component: DescargablesComponent},
  {path: 'Suscripcion', component: SuscripcionComponent},
  {path: 'Servicios', component: ServiciosComponent},
  {path: 'Tienda', component: TiendaComponent},
  {path: 'Contacto', component: ContactoComponent}
];
  
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})
  ],
  exports: [
    RouterModule
  ]
})  

export class AppRoutingModule { }
