import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

//IPR
import {MatBadgeModule, MatTabsModule, MatCardModule, MatGridListModule, MatListModule, MatDividerModule, MatDialogModule } from '@angular/material';
import { GoogleMapsModule } from '@angular/google-maps';
  

import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
//import { SampleModule } from 'app/main/sample/sample.module';
import { IndexComponent, MenuDialogCC, MenuDialogCCm, MenuDialogCDO, MenuDialogCDOm, MenuDialogFCL, MenuDialogFCLm, MenuDialogII, MenuDialogIIm, MenuDialogMC, MenuDialogMCm, MenuDialogMM, MenuDialogMMm, MenuDialogNHm, MenuDialogCTm, MenuDialogSD, MenuDialogSDm, MenuDialogSH, MenuDialogSHm, MenuDialogSUm, MenuDialogDSm, MenuDialogSRm } from './main/Index/index/index.component';
import { AppRoutingModule } from './app-routing.module';

/*IPR*/ 
import { DragScrollModule } from 'ngx-drag-scroll';
import { NgImageSliderModule } from 'ng-image-slider';
import { DescargablesComponent, MenuDialogDCC, MenuDialogDCDO, MenuDialogDFCL, MenuDialogDII, MenuDialogDMC, MenuDialogDMM, MenuDialogDSD, MenuDialogDSH } from './main/Descargables/descargables/descargables.component';
import { SuscripcionComponent, MenuDialogSCC, MenuDialogSMM, MenuDialogSCDO, MenuDialogSMC, MenuDialogSSH, MenuDialogSFCL, MenuDialogSII, MenuDialogSSD } from './main/Suscribete/suscripcion/suscripcion.component';
import { MenuDialogRCC, MenuDialogRCDO, MenuDialogRFCL, MenuDialogRII, MenuDialogRMC, MenuDialogRMM, MenuDialogRSD, MenuDialogRSH, ServiciosComponent } from './main/Servicios/servicios/servicios.component';
import { MenuTDialogCC, MenuTDialogCDO, MenuTDialogFCL, MenuTDialogII, MenuTDialogMC, MenuTDialogMM, MenuTDialogSD, MenuTDialogSH, TiendaComponent } from './main/Tienda/tienda/tienda.component';
import { ContactoComponent, MenuCDialogCC, MenuCDialogCDO, MenuCDialogFCL, MenuCDialogII, MenuCDialogMC, MenuCDialogMM, MenuCDialogSD, MenuCDialogSH } from './main/Contacto/contacto/contacto.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';

const appRoutes: Routes = [
    {
        path      : '**',
        redirectTo: 'sample'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        IndexComponent,
        DescargablesComponent,
        SuscripcionComponent,
        ServiciosComponent,
        TiendaComponent,
        ContactoComponent,
        //Index
        MenuDialogCC,
        MenuDialogMM,
        MenuDialogCDO,
        MenuDialogMC,
        MenuDialogSH,
        MenuDialogFCL,
        MenuDialogII,
        MenuDialogSD,

        //Contacto
        MenuCDialogCC,
        MenuCDialogMM,
        MenuCDialogCDO,
        MenuCDialogMC,
        MenuCDialogSH,
        MenuCDialogFCL,
        MenuCDialogII,
        MenuCDialogSD,

        //Tienda
        MenuTDialogCC,
        MenuTDialogMM,
        MenuTDialogCDO,
        MenuTDialogMC,
        MenuTDialogSH,
        MenuTDialogFCL,
        MenuTDialogII,
        MenuTDialogSD,
         
        //Index Movil
         MenuDialogCCm,
         MenuDialogMMm,
         MenuDialogCDOm,
         MenuDialogMCm,
         MenuDialogSHm,
         MenuDialogFCLm,
         MenuDialogIIm,
         MenuDialogSDm,
         MenuDialogNHm,
         MenuDialogCTm,
         MenuDialogSUm,
         MenuDialogDSm,
         MenuDialogSRm,
        //Suscripcion
        MenuDialogSCC,
        MenuDialogSMM,
        MenuDialogSCDO,
        MenuDialogSMC,
        MenuDialogSSH,
        MenuDialogSFCL,
        MenuDialogSII,
        MenuDialogSSD,
        
        //Descargables
        MenuDialogDCC,
        MenuDialogDMM,
        MenuDialogDCDO,
        MenuDialogDMC,
        MenuDialogDSH,
        MenuDialogDFCL,
        MenuDialogDII,
        MenuDialogDSD,
         //Servicios
         MenuDialogRCC,
         MenuDialogRMM,
         MenuDialogRCDO,
         MenuDialogRMC,
         MenuDialogRSH,
         MenuDialogRFCL,
         MenuDialogRII,
         MenuDialogRSD,
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,
        MatBadgeModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        MatCardModule,
        MatGridListModule,
        MatListModule,
        MatDividerModule,
        MatIconModule,
        GoogleMapsModule,
        MatDialogModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        //SampleModule,

        //IPR
        AppRoutingModule,
        DragScrollModule,
        NgImageSliderModule,
        CarouselModule.forRoot()

    ],
    bootstrap   : [
        AppComponent
    ],
    entryComponents:[
        //Index
        MenuDialogCC,
        MenuDialogMM,
        MenuDialogCDO,
        MenuDialogMC,
        MenuDialogSH,
        MenuDialogFCL,
        MenuDialogII,

        //Contacto
        MenuCDialogCC,
        MenuCDialogMM,
        MenuCDialogCDO,
        MenuCDialogMC,
        MenuCDialogSH,
        MenuCDialogFCL,
        MenuCDialogII,
        MenuCDialogSD,

         //Tienda
         MenuTDialogCC,
         MenuTDialogMM,
         MenuTDialogCDO,
         MenuTDialogMC,
         MenuTDialogSH,
         MenuTDialogFCL,
         MenuTDialogII,
         MenuTDialogSD,

        //Index Movil
        MenuDialogCCm,
        MenuDialogMMm,
        MenuDialogCDOm,
        MenuDialogMCm,
        MenuDialogSHm,
        MenuDialogFCLm,
        MenuDialogIIm,
        MenuDialogSDm,
        MenuDialogNHm,
        MenuDialogCTm,
        MenuDialogSUm,
        MenuDialogDSm,
        MenuDialogSRm,
        //Suscripcion
        MenuDialogSD,
        MenuDialogSCC,
        MenuDialogSMM,
        MenuDialogSCDO,
        MenuDialogSMC,
        MenuDialogSSH,
        MenuDialogSFCL,
        MenuDialogSII,
        MenuDialogSSD,
        //Descargables
        MenuDialogDCC,
        MenuDialogDMM,
        MenuDialogDCDO,
        MenuDialogDMC,
        MenuDialogDSH,
        MenuDialogDFCL,
        MenuDialogDII,
        MenuDialogDSD,
        //Servicios
        MenuDialogRCC,
        MenuDialogRMM,
        MenuDialogRCDO,
        MenuDialogRMC,
        MenuDialogRSH,
        MenuDialogRFCL,
        MenuDialogRII,
        MenuDialogRSD,
      ]
})
export class AppModule
{
}
