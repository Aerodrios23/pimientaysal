import { Component, ElementRef, Inject, NgModule, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { MatIconRegistry } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface DialogData 
{
  fianza: string;
  inclusion: string;
  comisionA: number;
  comisionB: number;
  primaNeta: number;
  comisionGeneral: number;
}

//********************** MENU ****************************************************/

export interface Ingredientes 
{
  Name: string;
}

export interface Images 
{
  Name: string;
}

export interface Platillos 
{
  Title: string;
  TitleP: string;
  ImgPla: string;
  Dificultad: string;
  Gluten: boolean;
  Lactosa: boolean;
  Dieta: string;
  Temporada: string;
  Tecnica: string;
  Tipo: string;
  UrlPdf: string;
  UrlYout: string;
  Preparacion: string;  
  Ingredientes: Array<Ingredientes>;
  Images: Array<Images>;
}


export interface Menu 
{
  MenuId: Number;
  Title: string;
  Platillos: Array<Platillos>;
}

@Component({
  selector: 'app-descargables',
  templateUrl: './descargables.component.html',
  styleUrls: ['./descargables.component.scss']
})
export class DescargablesComponent implements OnInit {

  //IPR
  title = 'app works!';
  hideScrollbar;
  disabled;
  xDisabled;
  yDisabled;
  imagelist = [
  'Menu2.jpg',
  'Menu3.jpg',
  'Menu4.jpg',
  'Menu2.jpg' ,
  'Menu3.jpg',
  'Menu4.jpg',
  'Menu2.jpg',
  'Menu3.jpg',
  'Menu4.jpg',
  'Menu2.jpg',
  ];
  leftNavDisabled = false;
  rightNavDisabled = false;
  index = 0;
  //vistas moviles
  nameMenu: string = "Nuestra Historia";
  isShowNh: boolean = true;
  isShowRh: boolean = false;
  isShowCc: boolean = false;
  isShowMm: boolean = false;
  isShowCd: boolean = false;
  isShowMc: boolean = false;
  isShowSh: boolean = false;
  isShowFl: boolean = false;
  isShowIi: boolean = false;
  isShowSd: boolean = false;
  isShowCt: boolean = false;
  isShowTi: boolean = false;

  isShowSus: boolean = false;
  isShowDes: boolean = false;
  isShowSrv: boolean = false;

  isSocNet: boolean = true;
  isSrv: boolean = true;

  private _jsonURL = 'assets/files/MasterFile.json';
  docs: any[];

  arrPlaCC:   Array<Platillos> = [];
  arrPlaMM:   Array<Platillos> = []; 
  arrPlaCDO:  Array<Platillos> = [];
  arrPlaMC:   Array<Platillos> = [];
  arrPlaSH:   Array<Platillos> = [];
  arrPlaFCL:  Array<Platillos> = [];
  arrPlaII:   Array<Platillos> = [];
  arrPlaSD:   Array<Platillos> = [];


  //Slider
  imageObjectMenu = [{
          image: 'assets/images/romeroYsal/CC/Menu1.jpg',
          thumbImage: 'assets/images/romeroYsal/CC/CC-Op1.png',
          title: 'Menu 1'
          }, {
              image: ' assets/images/romeroYsal/CC/Menu2.jpg',
              thumbImage: 'assets/images/romeroYsal/CC/CC-Op2.png',
              title: 'Menu 2'
          }, {
              image: 'assets/images/romeroYsal/CC/Menu3.jpg',
              thumbImage: 'assets/images/romeroYsal/CC/CC-Op1.png',
              title: 'Menu 3'
          },
  ];

  imageObject = [{
    image: 'assets/images/romeroYsal/Servicios/Servicios1.png',
    thumbImage: 'assets/images/romeroYsal/Servicios/Servicios1.png',
    title: 'Banquetes'
    }, {
        image: ' assets/images/romeroYsal/Servicios/Servicios2.png',
        thumbImage: 'assets/images/romeroYsal/Servicios/Servicios2.png',
        title: 'Cathering'
    }, {
        image: 'assets/images/romeroYsal/Servicios/Servicios3.png',
        thumbImage: 'assets/images/romeroYsal/Servicios/Servicios3.png',
        title: 'Decoración'
    },
];

  tiles: Tile[] = [

  {text: 'One',   cols: 2, rows: 2, color: ''},
  {text: 'Two',   cols: 1, rows: 1, color: ''},
  {text: 'Three', cols: 1, rows: 1, color: ''}

  ];


  dataCopi: DialogData;


  @ViewChild('nav', { read: DragScrollComponent, static: true }) ds: DragScrollComponent;

  @ViewChild('chkMenu', { static: false }) private chkMenu: ElementRef;

  constructor(
    matIconRegistry: MatIconRegistry,
    private router: Router,
    public dialog: MatDialog,
    private httpClient: HttpClient,
  
  ){

  }

  ngOnInit() {
    
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      this.docs = data[8];
      this.arrPlaCC = data[0].Platillos;

      this.arrPlaMM   =   data[1].Platillos;
      this.arrPlaCDO  =   data[2].Platillos;
      this.arrPlaMC   =   data[3].Platillos;
      this.arrPlaSH   =   data[4].Platillos;
      this.arrPlaFCL  =   data[5].Platillos;
      this.arrPlaII   =   data[6].Platillos;
      this.arrPlaSD   =   data[7].Platillos;

      this.showMenu(1);

    })

    

  }

  clickItem(item) {
  console.log('item clicked');
  }

  remove() {
    this.imagelist.pop();
  }

  toggleHideSB() {
    this.hideScrollbar = !this.hideScrollbar;
  }

  toggleDisable() {
    this.disabled = !this.disabled;
  }

  toggleXDisable() {
  this.xDisabled = !this.xDisabled;
  }

  toggleYDisable() {
  this.yDisabled = !this.yDisabled;
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  moveTo(idx: number) {
    this.ds.moveTo(idx);
  }

  leftBoundStat(reachesLeftBound: boolean) {
    this.leftNavDisabled = reachesLeftBound;
  }

  rightBoundStat(reachesRightBound: boolean) {
    this.rightNavDisabled = reachesRightBound;
  }

  onSnapAnimationFinished() {
    console.log('snap animation finished');
  }

  onIndexChanged(idx) {
    this.index = idx;
    console.log('current index: ' + idx);
  }

  onDragScrollInitialized() {
    console.log('first demo drag scroll has been initialized.');
  }

  onDragStart() {
    console.log('drag start');
  }

  onDragEnd() {
    console.log('drag end');
  }

  showMenu(id:number){

    //let inpMenu: HTMLElement = this.chkMenu.nativeElement;

    switch (id) {
      case 1:
        
        this.isShowNh  = true;
        this.isShowRh  = false;

        this.isShowCc   = false;
        this.isShowMm   = false;
        this.isShowCd   = false;
        this.isShowMc   = false;
        this.isShowSh   = false;
        this.isShowFl   = false;
        this.isShowIi   = false;
        this.isShowSd   = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Nuestra Historia";
        //inpMenu.click();
      
      break;

      case 2:
        this.isShowNh  = false;
        this.isShowRh  = true;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Ritual de lo Habitual";
        //inpMenu.click();
      break;

      case 3:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = true;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Corazón Contento";
        //inpMenu.click();
      break;

      case 4:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = true;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Mi Mero Mole";
        //inpMenu.click();
      break;

      case 5:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = true;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "El Comal le Dijo a la Olla";
        //inpMenu.click();
      break;

      case 6:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = true;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Mexicano de Corazón";
        //inpMenu.click();
      break;

      case 7:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = true;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Sabor de la Historia";
        //inpMenu.click();
      break;

      case 8:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = true;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Fresco Como Lechuga";
        //inpMenu.click();
      break;

      case 9:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = true;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Ingrediente Increíble";
        //inpMenu.click();
      break;

      case 10:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = true;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Siempre en Domingo";
        //inpMenu.click();
      break;
      case 11:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = true;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Contacto";
        //inpMenu.click();
      break;

      case 12:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = true;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        this.nameMenu = "Tienda";
        //inpMenu.click();
      break;

      case 13:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = true;
        this.isShowDes  = false;
        this.isShowSrv  = false;

        
        this.nameMenu = "Suscríbete";
        //inpMenu.click();
      break;

      case 14:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = true;
        this.isShowSrv  = false;

        this.nameMenu = "Descargables";
        //inpMenu.click();
      break;

      case 15:
        this.isShowNh  = false;
        this.isShowRh  = false;
        this.isShowCc  = false;
        this.isShowMm  = false;
        this.isShowCd  = false;
        this.isShowMc  = false;
        this.isShowSh  = false;
        this.isShowFl  = false;
        this.isShowIi  = false;
        this.isShowSd  = false;
        this.isShowCt   = false;
        this.isShowTi   = false;

        this.isShowSus  = false;
        this.isShowDes  = false;
        this.isShowSrv  = true;

        this.nameMenu = "Servicios";
        //inpMenu.click();
      break;
    
      default:
        break;
    }

  }

  closeImg(){
    this.isSocNet = true;
  }

  openImg(){
    this.isSocNet = false;
  }

  closeSrv(event){
    

    switch (event) {
      
      case 10:
        this.isSrv = true;
        this.router.navigate(["/Contacto"]).then( (e) => {
        });
        break;
      
      case 11:
        this.isSrv = false;
        this.router.navigate(["/Tienda"]).then( (e) => {
        });
        break;
    
      default:
        this.isSrv = false;
        break;
    }
  
  }


  closeMenu(){
    document.getElementById("chkMenu").click();
  }



  //************** OPEN DIALOG ********************************/
  //***********************************************************/
  openDialogCC(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDCC, {
      width: '800px', 
      height: '500px',
      data: { 
              platillo: this.arrPlaCC[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
      // if (result !== undefined)
      // {
      //   this.altaFianzaForm.patchValue({PORCENTAJE_COMISION_GO: result});
      // }
    });
  }

  //***********************************************************/

  openDialogMM(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDMM, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaMM[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogCDO(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDCDO, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaCDO[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogMC(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDMC, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaMC[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogSH(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDSH, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaSH[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogFCL(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDFCL, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaFCL[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogII(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDII, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaII[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //***********************************************************/

  openDialogSD(id:number): void {
    const dialogRef = this.dialog.open(MenuDialogDSD, {
      width: '800px', 
      height: '500px',
      data: { 
            platillo: this.arrPlaSD[id]
            }
    });

    dialogRef.afterClosed().subscribe(result => 
    {
  
    });
  }

  //************************************************************/

//***********************************************************/
//***********************************************************/

  GoSuscribete(){
    this.router.navigate(['Suscripcion']);
  }

  GoServicios(){
    this.router.navigate(['Servicios']);
  }

  GoDescargables(){
    this.router.navigate(['Descargables']);
  }

}


//************************ CREACION DE MENUS DESKTOP ********************************************************************************** */
@Component({
selector: 'MenuDialogCC',
templateUrl: '../../Menus/menuCC.html',
})
export class MenuDialogDCC {

menusRS: Menu;
//platilloObj: Platillos;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDCC>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogMM',
templateUrl: '../../Menus/menuMM.html',
})
export class MenuDialogDMM {

menusRS: Menu;
//platilloObj: Platillos;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDMM>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogCDO',
templateUrl: '../../Menus/menuCDO.html',
})
export class MenuDialogDCDO {

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDCDO>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogMC',
templateUrl: '../../Menus/menuMC.html',
})
export class MenuDialogDMC {

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDMC>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}
}

//*************************************************************/

@Component({
selector: 'MenuDialogSH',
templateUrl: '../../Menus/menuSH.html',
})
export class MenuDialogDSH {

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDSH>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogFCL',
templateUrl: '../../Menus/menuFCL.html',
})
export class MenuDialogDFCL {

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDFCL>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogII',
templateUrl: '../../Menus/menuII.html',
})
export class MenuDialogDII {

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDII>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}

}

//*************************************************************/

@Component({
selector: 'MenuDialogSD',
templateUrl: '../../Menus/menuSD.html',
})
export class MenuDialogDSD { 

menusRS: Menu;

dataCopi: DialogData;
arrPlatillos = [];
dataCopi2:  Array<Platillos> = []; 

private itemsPerSlide: number;
private _jsonURL = 'assets/files/MasterFile.json';
constructor(
  public dialogRef: MatDialogRef<MenuDialogDSD>,
  @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
  private httpClient: HttpClient,
  public router: Router,
){

  this.dataCopi2 = data;

}

ngOnInit(){

  let resultArrayString = JSON.stringify(this.data);
  let resultArrayObj =  JSON.parse(resultArrayString);

  this.arrPlatillos.push(resultArrayObj.platillo);

}

onNoClick(): void {
  this.dialogRef.close();
}

valuechange(event, data): void 
{
  this.dataCopi = data;
 
}
}

