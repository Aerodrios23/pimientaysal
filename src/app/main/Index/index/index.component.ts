import { Component, ElementRef, Inject, NgModule, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { MatIconRegistry } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface DialogData 
{
  fianza: string;
  inclusion: string;
  comisionA: number;
  comisionB: number;
  primaNeta: number;
  comisionGeneral: number;
}

//********************** MENU ****************************************************/

export interface Ingredientes 
{
  Name: string;
}

export interface Images 
{
  Name: string;
}

export interface Platillos 
{
  Title: string;
  TitleP: string;
  ImgPla: string;
  Dificultad: string;
  Gluten: boolean;
  Lactosa: boolean;
  Dieta: string;
  Temporada: string;
  Tecnica: string;
  Tipo: string;
  UrlPdf: string;
  UrlYout: string;
  Preparacion: string;  
  Ingredientes: Array<Ingredientes>;
  Images: Array<Images>;
}


export interface Menu 
{
  MenuId: Number;
  Title: string;
  Platillos: Array<Platillos>;
}


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  viewProviders: [MatIconRegistry]
})



export class IndexComponent implements OnInit {
    //IPR
    title = 'app works!';
    hideScrollbar;
    disabled;
    xDisabled;
    yDisabled;
    imagelist = [
    'Menu2.jpg',
    'Menu3.jpg',
    'Menu4.jpg',
    'Menu2.jpg' ,
    'Menu3.jpg',
    'Menu4.jpg',
    'Menu2.jpg',
    'Menu3.jpg',
    'Menu4.jpg',
    'Menu2.jpg',
    ];
    leftNavDisabled = false;
    rightNavDisabled = false;
    index = 0;
    //vistas moviles
    nameMenu: string = "Nuestra Historia";
    isShowNh: boolean = true;
    isShowRh: boolean = false;
    isShowCc: boolean = false;
    isShowMm: boolean = false;
    isShowCd: boolean = false;
    isShowMc: boolean = false;
    isShowSh: boolean = false;
    isShowFl: boolean = false;
    isShowIi: boolean = false;
    isShowSd: boolean = false;
    isShowCt: boolean = false;
    isShowTi: boolean = false;

    isShowSus: boolean = false;
    isShowDes: boolean = false;
    isShowSrv: boolean = false;

    isSocNet: boolean = true;
    isSrv: boolean = true;

    private _jsonURL = 'assets/files/MasterFile.json';
    docs: any[];

    arrPlaCC:   Array<Platillos> = [];
    arrPlaMM:   Array<Platillos> = []; 
    arrPlaCDO:  Array<Platillos> = [];
    arrPlaMC:   Array<Platillos> = [];
    arrPlaSH:   Array<Platillos> = [];
    arrPlaFCL:  Array<Platillos> = [];
    arrPlaII:   Array<Platillos> = [];
    arrPlaSD:   Array<Platillos> = [];


    //Slider
    imageObjectMenu = [{
            image: 'assets/images/romeroYsal/CC/Menu1.jpg',
            thumbImage: 'assets/images/romeroYsal/CC/CC-Op1.png',
            title: 'Menu 1'
            }, {
                image: ' assets/images/romeroYsal/CC/Menu2.jpg',
                thumbImage: 'assets/images/romeroYsal/CC/CC-Op2.png',
                title: 'Menu 2'
            }, {
                image: 'assets/images/romeroYsal/CC/Menu3.jpg',
                thumbImage: 'assets/images/romeroYsal/CC/CC-Op1.png',
                title: 'Menu 3'
            },
    ];

    imageObject = [{
      image: 'assets/images/romeroYsal/Servicios/Servicios1.png',
      thumbImage: 'assets/images/romeroYsal/Servicios/Servicios1.png',
      title: 'Banquetes'
      }, {
          image: ' assets/images/romeroYsal/Servicios/Servicios2.png',
          thumbImage: 'assets/images/romeroYsal/Servicios/Servicios2.png',
          title: 'Cathering'
      }, {
          image: 'assets/images/romeroYsal/Servicios/Servicios3.png',
          thumbImage: 'assets/images/romeroYsal/Servicios/Servicios3.png',
          title: 'Decoración'
      },
  ];

    tiles: Tile[] = [

    {text: 'One',   cols: 2, rows: 2, color: ''},
    {text: 'Two',   cols: 1, rows: 1, color: ''},
    {text: 'Three', cols: 1, rows: 1, color: ''}

    ];


    dataCopi: DialogData;


    @ViewChild('nav', { read: DragScrollComponent, static: true }) ds: DragScrollComponent;

    @ViewChild('chkMenu', { static: false }) private chkMenu: ElementRef;

    constructor(
      matIconRegistry: MatIconRegistry,
      private router: Router,
      public dialog: MatDialog,
      private httpClient: HttpClient,
    
    ){
  
    }

    ngOnInit() {
      
      this.httpClient.get(this._jsonURL).subscribe(data =>{
        this.docs = data[8];
        this.arrPlaCC = data[0].Platillos;

        this.arrPlaMM   =   data[1].Platillos;
        this.arrPlaCDO  =   data[2].Platillos;
        this.arrPlaMC   =   data[3].Platillos;
        this.arrPlaSH   =   data[4].Platillos;
        this.arrPlaFCL  =   data[5].Platillos;
        this.arrPlaII   =   data[6].Platillos;
        this.arrPlaSD   =   data[7].Platillos;

        this.showMenu(1);

      })

      

    }

    clickItem(item) {
    console.log('item clicked');
    }

    remove() {
      this.imagelist.pop();
    }

    toggleHideSB() {
      this.hideScrollbar = !this.hideScrollbar;
    }

    toggleDisable() {
      this.disabled = !this.disabled;
    }
  
    toggleXDisable() {
    this.xDisabled = !this.xDisabled;
    }
  
    toggleYDisable() {
    this.yDisabled = !this.yDisabled;
    }

    moveLeft() {
      this.ds.moveLeft();
    }

    moveRight() {
      this.ds.moveRight();
    }

    moveTo(idx: number) {
      this.ds.moveTo(idx);
    }

    leftBoundStat(reachesLeftBound: boolean) {
      this.leftNavDisabled = reachesLeftBound;
    }

    rightBoundStat(reachesRightBound: boolean) {
      this.rightNavDisabled = reachesRightBound;
    }

    onSnapAnimationFinished() {
      console.log('snap animation finished');
    }

    onIndexChanged(idx) {
      this.index = idx;
      console.log('current index: ' + idx);
    }

    onDragScrollInitialized() {
      console.log('first demo drag scroll has been initialized.');
    }

    onDragStart() {
      console.log('drag start');
    }

    onDragEnd() {
      console.log('drag end');
    }

    showMenu(id:number){

      //let inpMenu: HTMLElement = this.chkMenu.nativeElement;

      switch (id) {
        case 1:
          
          this.isShowNh  = true;
          this.isShowRh  = false;

          this.isShowCc   = false;
          this.isShowMm   = false;
          this.isShowCd   = false;
          this.isShowMc   = false;
          this.isShowSh   = false;
          this.isShowFl   = false;
          this.isShowIi   = false;
          this.isShowSd   = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Nuestra Historia";
          //inpMenu.click();
        
        break;

        case 2:
          this.isShowNh  = false;
          this.isShowRh  = true;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Ritual de lo Habitual";
          //inpMenu.click();
        break;

        case 3:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = true;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Corazón Contento";
          //inpMenu.click();
        break;

        case 4:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = true;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Mi Mero Mole";
          //inpMenu.click();
        break;

        case 5:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = true;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "El Comal le Dijo a la Olla";
          //inpMenu.click();
        break;

        case 6:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = true;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Mexicano de Corazón";
          //inpMenu.click();
        break;

        case 7:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = true;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Sabor de la Historia";
          //inpMenu.click();
        break;

        case 8:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = true;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Fresco Como Lechuga";
          //inpMenu.click();
        break;

        case 9:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = true;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Ingrediente Increíble";
          //inpMenu.click();
        break;

        case 10:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = true;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Siempre en Domingo";
          //inpMenu.click();
        break;
        case 11:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = true;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Contacto";
          //inpMenu.click();
        break;

        case 12:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = true;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          this.nameMenu = "Tienda";
          //inpMenu.click();
        break;

        case 13:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = true;
          this.isShowDes  = false;
          this.isShowSrv  = false;

          
          this.nameMenu = "Suscríbete";
          //inpMenu.click();
        break;

        case 14:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = true;
          this.isShowSrv  = false;

          this.nameMenu = "Descargables";
          //inpMenu.click();
        break;

        case 15:
          this.isShowNh  = false;
          this.isShowRh  = false;
          this.isShowCc  = false;
          this.isShowMm  = false;
          this.isShowCd  = false;
          this.isShowMc  = false;
          this.isShowSh  = false;
          this.isShowFl  = false;
          this.isShowIi  = false;
          this.isShowSd  = false;
          this.isShowCt   = false;
          this.isShowTi   = false;

          this.isShowSus  = false;
          this.isShowDes  = false;
          this.isShowSrv  = true;

          this.nameMenu = "Servicios";
          //inpMenu.click();
        break;
      
        default:
          break;
      }

    }

    closeImg(){
      this.isSocNet = true;
    }

    openImg(){
      this.isSocNet = false;
    }

    closeSrv(event){
      

      switch (event) {
        
        case 10:
          this.isSrv = false;
          this.router.navigate(["/Contacto"]).then( (e) => {
          });
          break;
        
        case 11:
          this.isSrv = false;
          this.router.navigate(["/Tienda"]).then( (e) => {
          });
          break;
      
        default:
          this.isSrv = false;
          break;
      }
    
    }


    closeMenu(){
      document.getElementById("chkMenu").click();
    }



    //************** OPEN DIALOG ********************************/
    //***********************************************************/
    openDialogCC(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogCC, {
        width: '800px', 
        height: '500px',
        data: { 
                platillo: this.arrPlaCC[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
        // if (result !== undefined)
        // {
        //   this.altaFianzaForm.patchValue({PORCENTAJE_COMISION_GO: result});
        // }
      });
    }

    openDialogCCm(): void {
      const dialogRef = this.dialog.open(MenuDialogCCm, {
        width: '800px', 
        height: '500px',
        data: { 

                // fianza: this.altaFianzaForm.get('NO_FIANZA').value, 
                // inclusion: this.altaFianzaForm.get('NO_INCLUSION').value,
                // comisionA: 70,
                // comisionB: 30,
                // comisionGeneral: this.altaFianzaForm.get('PORCENTAJE_COMISION').value,
                // primaNeta: this.altaFianzaForm.get('PRIMA_NETA').value
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
        // if (result !== undefined)
        // {
        //   this.altaFianzaForm.patchValue({PORCENTAJE_COMISION_GO: result});
        // }
      });
    }

    //***********************************************************/

    openDialogMM(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogMM, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaMM[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogMMm(): void {
      const dialogRef = this.dialog.open(MenuDialogMMm, {
        width: '800px', 
        height: '500px',
        data: { 
          
    
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    //***********************************************************/

    openDialogCDO(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogCDO, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaCDO[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogCDOm(): void {
      const dialogRef = this.dialog.open(MenuDialogCDOm, {
        width: '800px', 
        height: '500px',
        data: { 
          
    
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    //***********************************************************/

    openDialogMC(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogMC, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaMC[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogMCm(): void {
      const dialogRef = this.dialog.open(MenuDialogMCm, {
        width: '800px', 
        height: '500px',
        data: { 
          
        
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {

      });
    }

    //***********************************************************/

    openDialogSH(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogSH, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaSH[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogSHm(): void {
      const dialogRef = this.dialog.open(MenuDialogSHm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {

      });
    }

    //***********************************************************/

    openDialogFCL(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogFCL, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaFCL[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogFCLm(): void {
      const dialogRef = this.dialog.open(MenuDialogFCLm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {

      });
    }

    //***********************************************************/

    openDialogII(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogII, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaII[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogIIm(): void {
      const dialogRef = this.dialog.open(MenuDialogIIm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    //***********************************************************/

    openDialogSD(id:number): void {
      const dialogRef = this.dialog.open(MenuDialogSD, {
        width: '800px', 
        height: '500px',
        data: { 
              platillo: this.arrPlaSD[id]
              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogSDm(): void {
      const dialogRef = this.dialog.open(MenuDialogSDm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    //************************************************************/

    openDialogNHm(): void {
      const dialogRef = this.dialog.open(MenuDialogNHm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }


    openDialogCTm(): void {
      const dialogRef = this.dialog.open(MenuDialogCTm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }


    openDialogSUm(): void {
      const dialogRef = this.dialog.open(MenuDialogSUm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }

    openDialogDSm(): void {
      const dialogRef = this.dialog.open(MenuDialogDSm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }


    openDialogSRm(): void {
      const dialogRef = this.dialog.open(MenuDialogSRm, {
        width: '800px', 
        height: '500px',
        data: { 
          

              }
      });

      dialogRef.afterClosed().subscribe(result => 
      {
    
      });
    }


//***********************************************************/
//***********************************************************/

    GoSuscribete(){
      this.router.navigate(['Suscripcion']);
    }

    GoServicios(){
      this.router.navigate(['Servicios']);
    }

    GoDescargables(){
      this.router.navigate(['Descargables']);
    }

}


//************************ CREACION DE MENUS DESKTOP ********************************************************************************** */
@Component({
  selector: 'MenuDialogCC',
  templateUrl: '../../Menus/menuCC.html',
})
export class MenuDialogCC {

  menusRS: Menu;
  //platilloObj: Platillos;
 
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogCC>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogMM',
  templateUrl: '../../Menus/menuMM.html',
})
export class MenuDialogMM {

  menusRS: Menu;
  //platilloObj: Platillos;
 
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogCC>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogCDO',
  templateUrl: '../../Menus/menuCDO.html',
})
export class MenuDialogCDO {

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogCDO>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogMC',
  templateUrl: '../../Menus/menuMC.html',
})
export class MenuDialogMC {

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogMC>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }
}

//*************************************************************/

@Component({
  selector: 'MenuDialogSH',
  templateUrl: '../../Menus/menuSH.html',
})
export class MenuDialogSH {

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogSH>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogFCL',
  templateUrl: '../../Menus/menuFCL.html',
})
export class MenuDialogFCL {

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogFCL>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogII',
  templateUrl: '../../Menus/menuII.html',
})
export class MenuDialogII {

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogII>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogSD',
  templateUrl: '../../Menus/menuSD.html',
})
export class MenuDialogSD { 

  menusRS: Menu;
  
  dataCopi: DialogData;
  arrPlatillos = [];
  dataCopi2:  Array<Platillos> = []; 

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogSD>,
    @Inject(MAT_DIALOG_DATA) public data: Array<Platillos>,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi2 = data;

  }

  ngOnInit(){

    let resultArrayString = JSON.stringify(this.data);
    let resultArrayObj =  JSON.parse(resultArrayString);

    this.arrPlatillos.push(resultArrayObj.platillo);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }
}



//************************ CREACION DE MENUS MOVIL ********************************************************************************** */
@Component({
  selector: 'MenuDialogCCm',
  templateUrl: '../../Menus/menuCCm.html',
})
export class MenuDialogCCm {

  dataCopi: DialogData;
  menusRS: Menu;
  

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';
  constructor(
    public dialogRef: MatDialogRef<MenuDialogCCm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient,
    public router: Router,
  ){

    this.dataCopi = data;

  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      console.log(data);
      this.menusRS = data[0];

      let images = this.menusRS.Platillos[0].Images; 
      
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogMMm',
  templateUrl: '../../Menus/menuMMm.html',
})
export class MenuDialogMMm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[1];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogCDOm',
  templateUrl: '../../Menus/menuCDOm.html',
})
export class MenuDialogCDOm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[2];
 
    })
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogMCm',
  templateUrl: '../../Menus/menuMCm.html',
})
export class MenuDialogMCm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[3];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogSHm',
  templateUrl: '../../Menus/menuSHm.html',
})
export class MenuDialogSHm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[4];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogFCLm',
  templateUrl: '../../Menus/menuFCLm.html',
})
export class MenuDialogFCLm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[5];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogIIm',
  templateUrl: '../../Menus/menuIIm.html',
})
export class MenuDialogIIm {

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[6];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/

@Component({
  selector: 'MenuDialogSDm',
  templateUrl: '../../Menus/menuSDm.html',
})
export class MenuDialogSDm { 

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogMMm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[7];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}

//*************************************************************/  
@Component({
  selector: 'MenuDialogNHm',
  templateUrl: '../../Menus/menuNHm.html',
})
export class MenuDialogNHm { 

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogNHm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[7];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}


@Component({
  selector: 'MenuDialogCTm',
  templateUrl: '../../Menus/menuCTm.html',
})
export class MenuDialogCTm { 

  dataCopi: DialogData;
  menusRS: Menu;

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogCTm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[7];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}


@Component({
  selector: 'MenuDialogSUm',
  templateUrl: '../../Menus/menuSUm.html',
})
export class MenuDialogSUm { 

  dataCopi: DialogData;
  menusRS: Menu;
  docs: any[];

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogSUm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      console.log(data);
      this.menusRS = data[7];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}


@Component({
  selector: 'MenuDialogDSm',
  templateUrl: '../../Menus/menuDSm.html',
})
export class MenuDialogDSm { 

  dataCopi: DialogData;
  menusRS: Menu;
  docs: any[];

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogDSm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      //console.log(data);
      this.menusRS = data[7];
      this.docs = data[8];
 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}


@Component({
  selector: 'MenuDialogSRm',
  templateUrl: '../../Menus/menuSRm.html',
})
export class MenuDialogSRm { 

  dataCopi: DialogData;
  menusRS: Menu;
  docs: any[];

  private itemsPerSlide: number;
  private _jsonURL = 'assets/files/MasterFile.json';

  imageObject = [{
        image: 'assets/images/romeroYsal/V/Catering1.jpeg',
        thumbImage: 'assets/images/romeroYsal/V/Catering1.jpeg',
        title: 'Catering'
    }, {
        image: 'assets/images/romeroYsal/V/Catering2.jpeg',
        thumbImage: 'assets/images/romeroYsal/V/Catering2.jpeg',
        title: 'Cathering'
    }, {
        image: 'assets/images/romeroYsal/V/Banquete.jpeg',
        thumbImage: 'assets/images/romeroYsal/V/Banquete.jpeg',
        title: 'Banquete'
    },
    {
      image: 'assets/images/romeroYsal/V/Decoracion1.jpeg',
      thumbImage: 'assets/images/romeroYsal/V/Decoracion1.jpeg',
      title: 'Decoración'
    },
    {
    image: 'assets/images/romeroYsal/V/Decoracion2.jpeg',
    thumbImage: 'assets/images/romeroYsal/V/Decoracion2.jpeg',
    title: 'Decoración'
    },
];

  constructor(
  
    public dialogRef: MatDialogRef<MenuDialogSRm>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private httpClient: HttpClient
    
    ) {
      this.dataCopi = data;
  }

  ngOnInit(){
    this.httpClient.get(this._jsonURL).subscribe(data =>{
      
      //console.log(data);
      this.menusRS = data[7];

      // window.addEventListener("orientationchange", function() {                   
      //   if (window.matchMedia("(orientation: portrait)").matches) {
      //      alert("PORTRAIT")
      //    }
      //   if (window.matchMedia("(orientation: landscape)").matches) {
      //     alert("LANSCAPE")
      //    }
      // }, false);
      
    })


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  valuechange(event, data): void 
  {
    this.dataCopi = data;
   
  }

}
